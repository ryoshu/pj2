/* server.js */

const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const mdns = require('mdns');

var txtRecord = {
  name: 'led-server'
}

//var ad = mdns.createAdvertisement(mdns.tcp('http', 'led'), 8000, {txtRecord: txtRecord});
var ad = mdns.createAdvertisement(mdns.makeServiceType('_http._tcp,_led'), 8000, {txtRecord: txtRecord});
//var ad = mdns.createAdvertisement(mdns.tcp('led-server'), 8000, {txtRecord: txtRecord});
ad.start();

app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/index.html');
});

http.listen(8000, function(){
  console.log('listening on *:8000');
});

io.on('connection', function(socket) {
  var addr = socket['handshake']['address'];
  console.log('A client connected from ' + addr);
  socket.on('disconnect', function(){
    console.log('A client disconnected from ' + addr);
  });

  //receive an update event
  socket.on('update-color', function(msg) {
    var color = convertTo8Bit(msg.color);
    console.log('update-color: ' + msg.location + ":" + color.r);
    io.emit(msg.location, color);
  });
});

function convertTo8Bit(color) {
  return { r: parseInt(color.r * 255), g: parseInt(color.g * 255), b: parseInt(color.b * 255) };
}