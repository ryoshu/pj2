/* client.js */

const config = require('config');
const OPC = require('./lib/opc');
var spawn = require('child_process').spawn;
const mdns = require('mdns');

var client = new OPC('localhost', 7890);
var location = 'bartop';
var socket;

//console.log(__dirname + '/bin/start-osx');

//spawn('bash', [__dirname + '/bin/start-osx', __dirname + '/config/barbottom.json']);

var sequence = [
    mdns.rst.DNSServiceResolve(),
    'DNSServiceGetAddrInfo' in mdns.dns_sd ? mdns.rst.DNSServiceGetAddrInfo() : mdns.rst.getaddrinfo({families:[0]}),
    mdns.rst.makeAddressesUnique()
];
var browser = mdns.createBrowser(mdns.tcp('http'), {resolverSequence: sequence});

//var ad = mdns.createAdvertisement(mdns.tcp('led-client'), 8000);
//var browser = mdns.createBrowser(mdns.tcp('http'));

browser.on('serviceUp', function(service) {
  console.log("service up: ", service);
  //TODO: Figure out why subtypes aren't working
  if(service['txtRecord']['name'] && service['txtRecord']['name'] == 'led-server') {
    connectToLEDServer(service);
  }
});

browser.on('serviceDown', function(service) {
  console.log("service down: ", service);
});

browser.start();

function connectToLEDServer(service) {
  var url = 'http://' + service['host'] + ':' + service['port'];
  console.log('LED Server found at ' + url);
  socket = require("socket.io-client")(url);
  socket.on('connect', function () {
    console.log('Connected to LED server at ' + url);
    socket.on(location, function (color) {
  	  console.log(color);
  	  for(var i = 0; i < 512; ++i) {
        client.setPixel(i, color.r, color.g, color.b);
  	  }
  	  client.writePixels();
    });
  });  
}


