/* app.js */
$(function () {
  var socket = io();

  $("#bartop-power-buttons,#barbottom-power-buttons,#stage-power-buttons").buttonset();
  //$("#barbottom-power-buttons").buttonset();
  //$("#stage-power-buttons").buttonset();
  $("#tabs").tabs();
  
  $(".powerButtons").on('click', function (e) {
    if(e.target.id) {
      var msg, id, color, split;
      split = e.target.id.split('-');
      color = (split[1] == 'off') ? { r: 0, g: 0, b: 0} : { r: 1, g: 1, b: 1};
      msg = { location: split[0], color: color };
      socket.emit('update-color', msg);
    }
  });

  $('.colorpicker').on('slidermove sliderup', function (e) {
    var rgb, color, msg, split;
    rgb = $(this).wheelColorPicker('getColor');
    split = e.target.id.split('-');
    msg = { location: split[0], color: rgb };
    socket.emit('update-color', msg);
  });
  
  //prevent bouncing
  document.ontouchmove = function(event){
    event.preventDefault();
  }

  //prevent zooming on double tap
  $("body").nodoubletapzoom();
});

(function($) {
  $.fn.nodoubletapzoom = function() {
    $(this).bind('touchstart', function preventZoom(e){
      var t2 = e.timeStamp;
      var t1 = $(this).data('lastTouch') || t2;
      var dt = t2 - t1;
      var fingers = e.originalEvent.touches.length;
      $(this).data('lastTouch', t2);
      if (!dt || dt > 500 || fingers > 1) {
        return; // not double-tap
      }
      e.preventDefault(); // double tap - prevent the zoom
      // also synthesize click events we just swallowed up
      $(e.target).trigger('click');
    });
  };
})(jQuery);